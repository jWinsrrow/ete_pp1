# ETE PP1

Primera práctica de potencia, grupo ETE del proyecto HERMES.

El objetivo de la práctica es diseñar una fuente de alimentación dada una entrada continua en un rango de 7.5 a 16V 
con una salida fija de 5V y otra variable. (con un 7805 y un lm317 respectivamente) ambas con una salida de 1A.

----------

## Primeros pasos

Lo primero que debemos hacer a la hora de hacer una placa de circuito impreso es plantear bien que es lo que queremos diseñar y como.
En esta práctica nos han dado ya los componentes principales que necesitamos usar, así que podemos empezar por buscarlos en internet en una fuente fiable como el buscador de componentes [octopart](https://octopart.com) o distribuidores como [Digikey](https://www.digikey.es/).

Una vez hayamos buscado el component nos vamos a la página del fabricante y descargamos de ahí el datasheet. En mi caso he elegido los componentes de Texas instruments que además poseen datasheet online.

* [LM317](https://www.ti.com/document-viewer/LM317-N/datasheet)  
* [LM7805](https://www.ti.com/document-viewer/LM7800/datasheet)  

Una vez hecho esto nos podemos basar en la información del datasheet sobre como usar los componentes.  
Dibujamos los esquemas como nos lo indica el datasheet (Siempre entendiendolo, claro, si no no sirve de nada).  
Nos falta Por decidir como vamos a alimentar la placa y luego obtener la salida. Para ello he decidido emplear conectores de tornillo de 
dos terminales tanto para la entrada como para la salida, concretamente los conectores de 
[TE conectivity 1776275-2](https://www.te.com/usa-en/product-1776275-2.html)

Seleccionando el resto de componentes nos queda que tenemos:

* [condensadores de tantalo vishay 1uF 25V](https://www.vishay.com/product?docid=40002)
* [Diodos 1n4007 vishay](https://www.vishay.com/product?docid=88503)

## Calculos.

Tenemos dos "grupos" de cálculos necesarios a hacer:

* Cálculos de los valores de las resistencias.
* Cálculos térmicos.

### Cálculos de las resistencias.

Finalmente nos falta determinar las resistencias del lm317. Para ello podemos hacer varias cosas:

1. Usar la fórmula que nos da el datasheet de TI (y olvidarnos de todo lo demás).
2. Entender el circuito, como funciona y con eso calcular los valores.
3. Inventarnoslos (y a ver que pasa).

Como somos ingenieros nos olvidamos de la opción 3, y como además de eso queremos ser buenos ingenieros la opción 1 también la dejaremos un poco atrás, aunque nos servirá de referencia.  

El datasheet nos da la formula:

$$ V_{OUT} = 1.25V (1+\frac{R1}{R2}) + I_{ADJ}R2  $$

Pero porqué? de donde sale el 1.25V?  
En nuestro esquema tenemos que R1 es R1 pero R2 es RV1, ya que queremos que este valor pueda variar (para hacerlo ajustable).

El 1.25V es la tensión de referencia del LM317 y tratará de mantenerla entre los terminales ADJ y Vout, simplemente, eso es "todo" lo que hace.

Conociendo eso podemos forzar una corriente determinada en R1 y facilmente calculable mediante la ley de ohm ($ V = I \cdot R $).
dando valores:

$$ I = \frac{1.25}{240} = 5.2...\cdot 10^{-3} \approx 5\cdot 10^{-3}A = 5mA $$

Siendo la corriente de ajuste  entre $50$ y $100\mu A$ ($10^{-6}$) para esta aplicación podemos despreciar su influencia.

Así que tomaremos un valor de 3K3 para RV1 siguiendo los valores existentes (y comunes) de potenciometros (Serie E6) 
que nos da el mejor rango para llegar de 1.25 a $\approx 14.25V$ (16V entrada - 1.75V de caida en el LM317)

### Cálculos térmicos.

Para los cálculos térmicos usaremos el caso más desfaborable, en este caso $V_{IN} = 16V$ y $I_{MAX} = 1A$

Seguimos el esquema térmico equivalente:  

![](Readme_imgs/EsquemaTermicoEquivalente.drawio.svg)

**_LM7805_**:
Para el LM7805 la tensión de salida es de 5V por tanto tenemos una caida de tensión de 11V en el 7805, dando como potencia disipada @1A

$$ P_{Disipada} = 11V * 1A = 11W  $$  


$ t_{j_{MAX}} = 150ºC$  
$ t_{j_{Recomended MAX}} = 125ºC$  
$P_{D} = 11W $  
$R_{J-C} = 1.7 \frac{ºC}{W}$  (datasheet)  
$R_{C-H} \approx 0.15 \frac{ºC}{W} $  
$R_{H-A} = ?$   
$$ t_{AMB} = 25ºC$$


Planteamos la resolución y buscamos cual es el valor de resistencia disipador ambiente máxima que podemos asumir.

Aplicaremos un factor de seguridad del 20% a la temperatura de la unión, por tanto:

$ t_{j}  = 100ºC$

$$ R_{J-H}  = R_{J-C} + R_{C-H} = 1.7 + 0.15 = 1.85 \frac{ºC}{W} $$

$$  t = 11w * 1.85 + 25 =   45.35 ºC  $$

$$ R_{H-A_{MAX}} =  (100 - 45.35)/11  =  4.96...\frac{ºC}{W} \approx 5 \frac{ºC}{W} $$

Necesitamos por tanto un disipador de $5 \frac{ºC}{W} $ 


**_LM317_**:

Para el LM317 la tensión de salida minma es de 1.25V por tanto tenemos una caida de tensión de 14.75V en el 317, dando como potencia disipada @1A

$$ P_{Disipada} = 14.75V * 1A = 14.75W  $$  


$ t_{j_{MAX}} = 125ºC$  
$ t_{j_{Recomended MAX}} = 125ºC$  
$P_{D} = 14.75W $  
$R_{J-C} = 1.1 \frac{ºC}{W}$  (datasheet)  
$R_{C-H} \approx 0.15 \frac{ºC}{W} $  
$R_{H-A} = ?$   
$$ t_{AMB} = 25ºC$$

mismo procedimiento que antes:

$ t_{j}  = 100ºC$

$$ R_{J-H}  = R_{J-C} + R_{C-H} = 1.1 + 0.15 = 1.25 \frac{ºC}{W} $$

$$  t = 14.75w * 1.25 + 25 =   43.4375 ºC \approx 45 ºC $$

$$ R_{H-A_{MAX}} =  (100 - 45)/14.75 =  3.728...\frac{ºC}{W} \approx 3.7 \frac{ºC}{W} $$

Necesitamos por tanto un disipador de $3.7 \frac{ºC}{W} $ 


Buscando en octopart encontramos este disipador, de los cuales necesitaremos dos, uno para el 7805 y otro para el 317
[Disipador 3.5ºC/W ASSMANN](https://www.traceparts.com/els/assmann/es/product/assmann-wsw-components-extruded-heatsinks-with-solderpins-35kw-635x416x25mm-black-anodized-top3-to220-sot32-retaining-spring-v06hkt?CatalogPath=ASSMANN%3AASSMANN.020.040&Product=10-30032017-016043&PartNumber=V8511Z)
